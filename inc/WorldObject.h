#ifndef	WORLDOBJECT_H
#define WORLDOBJECT_H

// including standard headers
#include <iostream>

// including SDL headers
#include "SDL_render.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

// including project headers

class WorldObject
{
public:
	WorldObject();
	~WorldObject();

	void copyToRenderer();

	bool load( SDL_Renderer* renderer, std::string imgFileName );												// load texture from image
	bool load( SDL_Renderer* renderer, std::string text, TTF_Font* font, SDL_Color color );						// load texture from rendered string
	
	uint16_t getPosX();
	uint16_t getPosY();
	uint16_t getCenterX();
	uint16_t getCenterY();
	uint16_t getWidth();
	uint16_t getHeight();
	double getSpeedX();
	double getSpeedY();
	void setPosX( uint16_t x );
	void setPosY( uint16_t y );
	void setCenter( uint16_t cx, uint16_t cy );
	void setSize( uint16_t w, uint16_t h );
	void setSpeed( double vx, double vy );

private:
	SDL_Renderer* mRenderer;
	SDL_Texture* mTexture;

	uint16_t mPosX;
	uint16_t mPosY;
	uint16_t mCenterX;
	uint16_t mCenterY;
	uint16_t mWidth;
	uint16_t mHeight;
	uint16_t mSpeedX;
	uint16_t mSpeedY;
	
	void free();
};

#endif // WORLDOBJECT_H
