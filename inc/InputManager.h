#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

// including standard headers
#include <iostream>

// including SDL headers
#include "SDL_events.h"

// including project headers

class InputManager
{
public:
	InputManager();
	~InputManager();

	bool initialize();
	void terminate();
	void handleInput();
	bool isKeyPressed( uint16_t key );
	bool isWindowQuit();

	uint16_t getMousePositionX();
	uint16_t getMousePositionY();

	// enum of keys
	enum key
	{
		KEY_ESCAPE = SDL_SCANCODE_ESCAPE,
	};

private:
	bool mIsKeyPressed[SDL_NUM_SCANCODES];	// handles the status of all keyboard keys
	bool mIsWindowQuit;
	uint16_t mMousePositionX;
	uint16_t mMousePositionY;
};

#endif // INPUTMANAGER_H
