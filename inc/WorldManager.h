#ifndef WORLDMANAGER_H
#define WORLDMANAGER_H

// including standard headers
#include <iostream>
#include <vector>

// including SDL headers

// including project headers
#include "InputManager.h"
#include "WorldObject.h"

class WorldManager
{
public:
	WorldManager();
	~WorldManager();

	bool initialize( SDL_Renderer* renderer );
	void terminate();
	void update();
	void draw();
	bool loadWorld();

private:
	SDL_Renderer* mRenderer;
	
	WorldObject mBackgroundObject;
	std::vector<WorldObject*> mObjectVector;
};

#endif // WORLDMANAGER_H
