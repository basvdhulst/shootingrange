#ifndef GUIMANAGER_H
#define GUIMANAGER_H

// including standard headers
#include <iostream>

// including SDL headers
#include "SDL_ttf.h"

// including project headers
#include "InputManager.h"
#include "WorldManager.h"
#include "WorldObject.h"

// REMOVE
#include <sstream>

class GuiManager
{
public:
	GuiManager();
	~GuiManager();

	bool initialize( SDL_Renderer* renderer );
	void terminate();
	void update();
	void draw();
	void interpolate( const double interpolate );

	void setFPSText( uint16_t fps );
	void setCursorPosition( uint16_t x, uint16_t y );

private:
	SDL_Renderer* mRenderer;
	TTF_Font* mDefaultFont;
	
	uint16_t mFPS;
	
	// image objects
	WorldObject mFPSText;
	WorldObject mCursor;
};

#endif // GUIMANAGER_H
