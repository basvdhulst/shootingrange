#ifndef APPLICATION_H
#define APPLICATION_H

// including standard headers
#include <iostream>

// including SDL headers
#include "SDL_video.h"
#include "SDL_render.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "SDL_timer.h"

// including project headers
#include "InputManager.h"
#include "WorldManager.h"
#include "GuiManager.h"

class Application
{
public:
	Application();
	~Application();

	bool initialize();
	void terminate();
	void run();
	void gameLoop();
	void updateGameLogic();

private:
	SDL_Window* mWindow;
	SDL_Renderer* mRenderer;
	
	InputManager mInputManager;
	WorldManager mWorldManager;	
	GuiManager mGuiManager;

	const uint32_t mScreenWidth;
	const uint32_t mScreenHeight;
	bool mIsShutdownRequested;

	void render();
};

#endif // APPLICATION_H
