// including header file
#include "Application.h"

Application::Application() :
	mWindow(NULL),
	mRenderer(NULL),
	mScreenWidth(1440),
	mScreenHeight(900),
	mIsShutdownRequested(true)
{
}

Application::~Application()
{
}

bool Application::initialize()
{
	bool success = true;

	// initialize SDL, SDL_image and SDL_TTF, and create window and renderer
	
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		std::cout << "SDL could not initialize! SDL_ERROR: " << SDL_GetError() << std::endl;
		success = false;
	}
	else
	{
		mWindow = SDL_CreateWindow( "Shooting Range!", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
									mScreenWidth, mScreenHeight, SDL_WINDOW_SHOWN );								//, SDL_WINDOW_FULLSCREEN_DESKTOP );
		
		if( mWindow == NULL )
		{
			std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			mRenderer = SDL_CreateRenderer( mWindow, -1, SDL_RENDERER_ACCELERATED |
											SDL_RENDERER_PRESENTVSYNC );

			if( mRenderer == NULL )
			{
				std::cout << "Renderer could not be created! SDL_Error: " << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor( mRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				
				int imgFlags = IMG_INIT_JPG;

				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					std::cout << "SDL_image could not initialize! SDL_image error: " << IMG_GetError()
								<< std::endl;
					success = false;
				}

				if( TTF_Init() == -1 )
				{
					std::cout << "SDL_ttf could not initialize! SDL_ttf Error: " << TTF_GetError()
								<< std::endl;
					success = false;
				}
			}
		}
	}

	// initialize all managers
	success = mInputManager.initialize();
	success = mWorldManager.initialize( mRenderer );
	success = mGuiManager.initialize( mRenderer );

	if( success )
	{
		mIsShutdownRequested = false;
	}

	return success;
}

void Application::terminate()
{
	// terminate all managers
	mGuiManager.terminate();
	mWorldManager.terminate();
	mInputManager.terminate();

	// Destroy renderer and window
	SDL_DestroyRenderer( mRenderer );
	SDL_DestroyWindow( mWindow );

	// set all pointers to NULL
	mRenderer = NULL;
	mWindow = NULL;

	// Terminate all systems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

void Application::run()
{
	// set-up game world
	mWorldManager.loadWorld();
	
	// start game loop
	gameLoop();

	// Close game world
}

void Application::gameLoop()
{
	uint32_t framesFromStart = 0;
	uint32_t ticksFromStart = SDL_GetTicks();	

	// timer variables - game update loop
	const int8_t ticksWaitForUpdate = 1000 / 25;
	const int8_t maxFrameSkip = 5;
	uint32_t ticksNextUpdate = ticksFromStart;

	// timer variables - FPS calculation	
	const int16_t ticksWaitForFPSCheck = 1000 / 1;
	uint16_t framesLastFPSCheck = framesFromStart;
	uint32_t ticksNextFPSCheck = ticksFromStart;
	
	while( !mIsShutdownRequested )
	{
		uint8_t framesSkipped = 0;
		uint32_t ticksStartFrame = SDL_GetTicks();
		framesFromStart++;

		// handle input as fast as possible to prevent mouse lag
		mInputManager.handleInput();
		
		while( SDL_TICKS_PASSED( ticksStartFrame, ticksNextUpdate ) && framesSkipped < maxFrameSkip )
		{
			// update all managers
			updateGameLogic();
			mWorldManager.update();
			mGuiManager.update();
		
			// update loop variables	
			ticksNextUpdate += ticksWaitForUpdate;
			framesSkipped++;
		}
	
		// use interpolation here
		const double interpolate = (double)( ticksNextUpdate - SDL_GetTicks() ) / ticksWaitForUpdate;
		mGuiManager.interpolate( interpolate );

		// render to screen
		render();

		// calculate FPS
		if( SDL_TICKS_PASSED( ticksStartFrame, ticksNextFPSCheck ) )
		{
			uint16_t fps = 1000 * ( framesFromStart - framesLastFPSCheck ) / ticksWaitForFPSCheck;
			mGuiManager.setFPSText( fps );

			framesLastFPSCheck = framesFromStart;
			ticksNextFPSCheck += ticksWaitForFPSCheck;
		}
	}
}

void Application::render()
{
	// clear renderer screen
	SDL_SetRenderDrawColor( mRenderer, 0, 0, 0, 0 );
	SDL_RenderClear( mRenderer );
	
	// draw game objects
	mWorldManager.draw();
	mGuiManager.draw();

	// swap renderer buffer
	SDL_RenderPresent( mRenderer );
}

void Application::updateGameLogic()
{
	// redirect mouse events
	mGuiManager.setCursorPosition( mInputManager.getMousePositionX(), mInputManager.getMousePositionY() );
	
	// redirect key events
	if( mInputManager.isKeyPressed( InputManager::KEY_ESCAPE ) || mInputManager.isWindowQuit() )
	{
		mIsShutdownRequested = true;
	}
}
