// including header file
#include "InputManager.h"

InputManager::InputManager() :
	mIsWindowQuit(false),
	mMousePositionX(0),
	mMousePositionY(0)
{
}

InputManager::~InputManager()
{
}

bool InputManager::initialize()
{
	bool success = true;

	for( int i = 0; i < SDL_NUM_SCANCODES; ++i )
	{
		mIsKeyPressed[i] = false;	
	}

	return success;
}

void InputManager::terminate()
{
}

void InputManager::handleInput()
{
	SDL_Event event;

	while( SDL_PollEvent( &event ) )
	{
		switch( event.type )
		{
			case SDL_MOUSEMOTION:
				mMousePositionX = event.motion.x;
				mMousePositionY = event.motion.y;
				break;
			case SDL_KEYDOWN:
				mIsKeyPressed[event.key.keysym.scancode] = true;
				break;
			case SDL_KEYUP:
				mIsKeyPressed[event.key.keysym.scancode] = false;
				break;
			case SDL_QUIT:
				mIsWindowQuit = true;
				break;
			default:
				// do nothing
				break;
		}
	}
}

bool InputManager::isKeyPressed( uint16_t key )
{
	return mIsKeyPressed[key];
}

bool InputManager::isWindowQuit()
{
	return mIsWindowQuit;
}

uint16_t InputManager::getMousePositionX()
{
	return mMousePositionX;
}

uint16_t InputManager::getMousePositionY()
{
	return mMousePositionY;
}
