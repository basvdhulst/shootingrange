// including header file
#include "WorldObject.h"

WorldObject::WorldObject() :
	mRenderer(NULL),
	mTexture(NULL),
	mPosX(0),
	mPosY(0),
	mCenterX(0),
	mCenterY(0),
	mWidth(0),
	mHeight(0),
	mSpeedX(0),
	mSpeedY(0)
{
}

WorldObject::~WorldObject()
{
	free();
}

void WorldObject::free()
{
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
	}

	mRenderer = NULL;
	mTexture = NULL;
	mPosX = 0;
	mPosY = 0;
	mCenterX = 0;
	mCenterY = 0;
	mWidth = 0;
	mHeight = 0;
	mSpeedX = 0;
	mSpeedY = 0;
}

void WorldObject::copyToRenderer()
{
	SDL_Rect renderRect = { mPosX, mPosY, mWidth, mHeight };

	if( mRenderer && mTexture )
	{
		SDL_RenderCopy( mRenderer, mTexture, NULL, &renderRect );
	}
}

bool WorldObject::load( SDL_Renderer* renderer, std::string imgFileName )
{
	// free previous resources when function is called a second time
	free();

	// set renderer of texture
	mRenderer = renderer;

	SDL_Surface* surface = IMG_Load( imgFileName.c_str() );

	if( surface == NULL)
	{
		std::cout << "Unable to load image " << imgFileName.c_str() << "! SDL error: " << SDL_GetError()
					<< std::endl;
	}
	else
	{
		mTexture = SDL_CreateTextureFromSurface( mRenderer, surface );

		if( mTexture == NULL )
		{
			std::cout << "Unable to create texture from surface " << imgFileName.c_str() << "! SDL error: "
						<< SDL_GetError() << std::endl;
		}
		else
		{
			mWidth = surface->w;
			mHeight = surface->h;
		}

		SDL_FreeSurface( surface );
	}

	return (mTexture != NULL);
}

bool WorldObject::load( SDL_Renderer* renderer, std::string text, TTF_Font* font, SDL_Color color )
{	
	// free previous resources when function is called a second time
	free();

	// set renderer of texture
	mRenderer = renderer;

	SDL_Surface* surface = TTF_RenderText_Solid( font, text.c_str(), color );
	
	if( surface == NULL)
	{
		std::cout << "Unable to render text from font! TTF error: " << TTF_GetError() << std::endl;
	}
	else
	{
		mTexture = SDL_CreateTextureFromSurface( mRenderer, surface );

		if( mTexture == NULL )
		{
			std::cout << "Unable to create texture from surface for font " << text.c_str() << "! SDL error: "
						<< SDL_GetError() << std::endl;
		}
		else
		{
			mWidth = surface->w;
			mHeight = surface->h;
		}

		SDL_FreeSurface( surface );
	}

	return (mTexture != NULL);
}

uint16_t WorldObject::getPosX()
{
	return mPosX;
}

uint16_t WorldObject::getPosY()
{
	return mPosY;
}

uint16_t WorldObject::getCenterX()
{
	return mCenterX;
}

uint16_t WorldObject::getCenterY()
{
	return mCenterY;
}

uint16_t WorldObject::getWidth()
{
	return mWidth;
}

uint16_t WorldObject::getHeight()
{
	return mHeight;
}

double WorldObject::getSpeedX()
{
	return mSpeedX;
}

double WorldObject::getSpeedY()
{
	return mSpeedY;
}

void WorldObject::setPosX( uint16_t x )
{
	mPosX = x - mCenterX;
}

void WorldObject::setPosY( uint16_t y )
{
	mPosY = y - mCenterY;
}

void WorldObject::setCenter( uint16_t cx, uint16_t cy )
{
	mCenterX = cx;
	mCenterY = cy;
}

void WorldObject::setSize( uint16_t w, uint16_t h )
{
	mWidth = w;
	mHeight = h;
}

void WorldObject::setSpeed( double vx, double vy )
{
	mSpeedX = vx;
	mSpeedY = vy;
}
