// including header file
#include "WorldManager.h"

WorldManager::WorldManager() :
	mRenderer(NULL)
{
}

WorldManager::~WorldManager()
{
}

bool WorldManager::initialize( SDL_Renderer* renderer )
{
	mRenderer = renderer;

	return true;
}

void WorldManager::terminate()
{
	for( std::vector<WorldObject*>::iterator it = mObjectVector.begin(); it != mObjectVector.end(); ++it )
	{
		delete (*it);
	}

	mObjectVector.clear();
	
	// set all pointers to NULL
	mRenderer = NULL;
}

void WorldManager::update()
{
	//if( mInputManager->isKeyPressed( InputManager::KEY_W ) )
	//{
	//	WorldObject* texture = new WorldObject;
	//	texture->load( mRenderer, "balloon.png" );
	//	mObjectVector.push_back( texture );
	//}
}

void WorldManager::draw()
{
	// copy all textures to renderer
	mBackgroundObject.copyToRenderer();

	for( std::vector<WorldObject*>::iterator it = mObjectVector.begin(); it != mObjectVector.end(); ++it )
	{
		(*it)->copyToRenderer();
	}
}

bool WorldManager::loadWorld()
{
	bool success = true;

	std::string imgFile = "city.jpg";
	
	if( !mBackgroundObject.load( mRenderer, imgFile.c_str() ) )
	{
		std::cout << "Unable to load texture from file " << imgFile.c_str() << std::endl;
		success = false;
	}

	return success;
}
