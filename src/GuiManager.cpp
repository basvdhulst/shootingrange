// including header file
#include "GuiManager.h"

GuiManager::GuiManager() :
	mRenderer(NULL),
	mDefaultFont(NULL),
	mFPS(0)
{
}

GuiManager::~GuiManager()
{
}

bool GuiManager::initialize( SDL_Renderer* renderer )
{
	bool success = true;

	mRenderer = renderer;

	std::string fontName = "lazy.ttf";
	mDefaultFont = TTF_OpenFont( fontName.c_str(), 28 );
	
	if( mDefaultFont == NULL )
	{
		std::cout << "Failed to load font " << fontName << "! SDL Error: " << SDL_GetError()  << std::endl;
		success = false;
	}
	
	// load cursor
	success = mCursor.load( mRenderer, "cursor.png" );
	mCursor.setSize( 50, 50 );
	mCursor.setCenter( 25, 25 );
	SDL_ShowCursor( SDL_DISABLE );

	return success;
}

void GuiManager::terminate()
{
	// close fonts
	TTF_CloseFont( mDefaultFont );

	// set all pointers to NULL
	mRenderer = NULL;
	mDefaultFont = NULL;
	mFPS = 0;
}

void GuiManager::update()
{

}

void GuiManager::draw()
{
	// copy all textures to renderer	
	mFPSText.copyToRenderer();
	mCursor.copyToRenderer();
}

void GuiManager::interpolate( const double interpolate )
{
}

void GuiManager::setFPSText( uint16_t fps )
{
	if( fps != mFPS )
	{
		SDL_Color color = { 255, 255, 255, 255 };
		std::stringstream ss;
		ss << mFPS;
		mFPSText.load( mRenderer, ss.str(), mDefaultFont, color ); 
		mFPS = fps;
	}
}

void GuiManager::setCursorPosition( uint16_t x, uint16_t y )
{
	mCursor.setPosX( x );
	mCursor.setPosY( y );
}
